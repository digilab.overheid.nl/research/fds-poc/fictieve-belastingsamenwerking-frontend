package model

import "github.com/google/uuid"

type Tariff struct {
	ID                                 uuid.UUID `json:"id"`
	RunID                              uuid.UUID `json:"runId"`
	Municipality                       string    `json:"municipality"`
	PropertyTaxOwnershipResidential    float64   `json:"propertyTaxOwnershipResidential"`
	PropertyTaxOwnershipNonResidential float64   `json:"propertyTaxOwnershipNonResidential"`
	PropertyTaxUsageNonResidential     float64   `json:"propertyTaxUsageNonResidential"`
	SewerageTax                        float64   `json:"sewerageTax"`
	WasteTaxOnePersonHousehold         float64   `json:"wasteTaxOnePersonHousehold"`
	WasteTaxMultiPersonHousehold       float64   `json:"wasteTaxMultiPersonHousehold"`
}
