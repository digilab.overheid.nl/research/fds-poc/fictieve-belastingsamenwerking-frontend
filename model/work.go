package model

import "time"

type Work struct {
	Amount     int           `json:"amount"`
	Done       int           `json:"done"`
	Error      int           `json:"error"`
	Finished   bool          `json:"finished"`
	CreatedAt  time.Time     `json:"-"`
	FinishedIn time.Duration `json:"finishedIn"`
}
