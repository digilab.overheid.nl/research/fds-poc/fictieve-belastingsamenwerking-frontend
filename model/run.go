package model

import (
	"time"

	"github.com/google/uuid"
)

type Status string

const (
	RunStatusInactive        Status = "inactive"
	RunStatusBatchRunRunning Status = "batch_run_running"
	RunStatusBatchRunDone    Status = "batch_run_done"
	RunStatusFinalizeRunning Status = "finalize_running"
	RunStatusFinalizeDone    Status = "finalize_done"
)

type Run struct {
	ID        uuid.UUID `json:"id"`
	Year      int       `json:"year"`
	Status    Status    `json:"status"`
	CreatedAt time.Time `json:"createdAt"`
}

func (item Status) String() string {
	switch item {
	case RunStatusInactive:
		return "Inactive"
	case RunStatusBatchRunRunning:
		return "Batch run bezig"
	case RunStatusBatchRunDone:
		return "Batch run klaar"
	case RunStatusFinalizeRunning:
		return "Aanslagbiljetten Bezig"
	case RunStatusFinalizeDone:
		return "Aanslagbiljetten Klaar"
	default:
		return ""
	}
}
