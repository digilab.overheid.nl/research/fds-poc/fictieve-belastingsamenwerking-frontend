package model

import (
	"time"

	"github.com/google/uuid"
)

type TaxBill struct {
	ID         uuid.UUID `json:"id"`
	RunID      uuid.UUID `json:"runId"`
	OwnerType  string    `json:"ownerType"`
	OwnerID    uuid.UUID `json:"ownerId"`
	OwnerLabel string    `json:"ownerLabel"`
	CreatedAt  time.Time `json:"createdAt"`
}
