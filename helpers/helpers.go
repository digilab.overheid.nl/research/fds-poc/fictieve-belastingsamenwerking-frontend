package helpers

import (
	"log"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

var printer = message.NewPrinter(language.Dutch)

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

// amsLocation refers to the Dutch time zone / location
var amsLocation *time.Location

// FormattedTime is used to unmarshal datetime in Dutch format directly from POST bodies
type FormattedTime time.Time

func init() {
	var err error
	if amsLocation, err = time.LoadLocation("Europe/Amsterdam"); err != nil {
		log.Fatalf("error getting time zone: %v", err)
	}
}

type Stringer interface {
	String() string
}

func FormatString(t any) string {
	if t, ok := t.(Stringer); ok {
		return t.String()
	}
	return ""

}

// FormatTime formats the specified time in the Dutch time zone and language
func FormatTime(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006 15:04"))
}

// FormatDate formats the specified time in the Dutch time zone and language
func FormatDate(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006"))
}

// FormatYear returns the year of the specief time.Time instance in the Dutch time zone
func FormatYear(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return strconv.Itoa(t.In(amsLocation).Year())
}

// NumberFormat formats the specified number as string. IMPROVE: access an interface argument (or generic) and support multiple number types
func NumberFormat(v interface{}) string {
	if v == nil {
		return ""
	}

	switch v.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64:
		// Integer: show the number without decimals
		return printer.Sprint(v)
	default:
		// Otherwise (e.g. float): assume the number is a currency, so show the number with two fixed digits
		return printer.Sprint(number.Decimal(v, number.MinFractionDigits(2), number.MaxFractionDigits(2)))
	}

	// IMPROVE: use a library like github.com/shopspring/decimal for decimal numbers instead of float64, see https://github.com/shopspring/decimal#why-dont-you-just-use-float64
}
