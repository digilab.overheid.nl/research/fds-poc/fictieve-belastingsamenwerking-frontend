package backend

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// Request performs a simple http request to the given path and unmarshals the result to the specified pointer
func Request(method string, baseURL string, path string, data interface{}, extraRequestHeaders map[string]string, v interface{}) (err error) {
	client := http.Client{}
	var req *http.Request

	var jsonData []byte
	if data != nil {
		if jsonData, err = json.Marshal(data); err != nil {
			return
		}
	}

	if req, err = http.NewRequest(method, baseURL+path, bytes.NewBuffer(jsonData)); err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	for key, value := range extraRequestHeaders {
		req.Header.Set(key, value)
	}

	// Do the request
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		return
	}

	if resp.StatusCode >= http.StatusMultipleChoices {
		body, _ := io.ReadAll(resp.Body)
		err = fmt.Errorf("response returned with error code %v. response body: %v", resp.StatusCode, string(body))
		return
	}

	// Read the response body and possible error
	var result []byte
	if result, err = io.ReadAll(resp.Body); err != nil {
		return
	}

	// If v is set, unmarshal the response to v (which should be a pointer)
	if v != nil {
		err = json.Unmarshal(result, v)
	}

	return
}

func Download(method string, baseURL string, path string, extraRequestHeaders map[string]string, dst io.Writer) error {
	client := http.Client{}

	req, err := http.NewRequest(method, baseURL+path, nil)
	if err != nil {
		return fmt.Errorf("new request failed: %w", err)
	}

	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	for key, value := range extraRequestHeaders {
		req.Header.Set(key, value)
	}

	// Do the request
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		return fmt.Errorf("request do failed: %w", err)
	}

	if resp.StatusCode >= http.StatusMultipleChoices {
		body, _ := io.ReadAll(resp.Body)
		return fmt.Errorf("response returned with error code %v. response body: %v", resp.StatusCode, string(body))
	}

	if _, err := io.Copy(dst, resp.Body); err != nil {
		return fmt.Errorf("copy data failed: %w", err)
	}

	return nil
}
