package config

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Municipality struct {
	ID   string `yaml:"id"`
	Name string `yaml:"name"`
	WOZ  struct {
		Domain       string `yaml:"domain"`
		FSCGrantHash string `yaml:"fscGrantHash"`
	} `yaml:"woz"`
}

var Config struct {
	Municipalities []Municipality `yaml:"municipalities"`
	Backend        struct {
		Domain       string `yaml:"domain"`
		FSCGrantHash string `yaml:"fscGrantHash"`
	} `yaml:"backend"`
	ListenAddress string `yaml:"listenAddress"`
}

func init() {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "config/dev.yaml"
	}

	yamlFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatalf("error reading config file: %v", err)
	}

	if err = yaml.Unmarshal(yamlFile, &Config); err != nil {
		log.Fatalf("error unmarshalling config: %v", err)
	}

	if Config.ListenAddress == "" {
		Config.ListenAddress = "0.0.0.0:80" // Default port if not set in config
	}
}

func GetMunicipalityByID(id string) (municipality Municipality, err error) {
	for _, m := range Config.Municipalities {
		if m.ID == id {
			municipality = m
			return
		}
	}

	err = fmt.Errorf("no municipality in config with ID '%s'", id)
	return
}
