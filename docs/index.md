---
title : "Fictieve belastingsamenwerking frontend"
description: "Frontend to visualize data in registries that tax collaboration organizations have access to, scoped to the WOZ."
date: 2023-10-27T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go
```

This starts a web server locally on the address specified in the config.


# Config

Use the `CONFIG_PATH` environment variable to define which config file to load (by default: config/dev.yaml).


## Backends

This service expects that the WOZ objecten backend for each municipality and FRP backend are accessible, the endpoints of which can be configured using the config.


### Hot reload

To enable hot reload when e.g. a view (template) changes, install a tool like [entr](https://github.com/eradman/entr) and run e.g.
```
find views/ | entr -r go run main.go
```
