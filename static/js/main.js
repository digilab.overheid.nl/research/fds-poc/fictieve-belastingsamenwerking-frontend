// Imports
import './htmx.js'; // Note: imported this way, since some plugins need the global `htmx` variable, which requires the use of an extra file according to https://htmx.org/docs/#webpack (which indeed seems to be the case)
import './htmx-sse.js';