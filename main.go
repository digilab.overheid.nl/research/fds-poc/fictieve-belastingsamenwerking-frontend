package main

import (
	"bufio"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"

	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-frontend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-frontend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-frontend/helpers"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-frontend/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingsamenwerking-frontend/session"
)

type specs struct {
	PropertyTaxOwnershipHome     float64 `json:"propertyTaxOwnershipResidential"`
	PropertyTaxOwnershipNonHome  float64 `json:"propertyTaxOwnershipNonResidential"`
	PropertyTaxUsageNonHome      float64 `json:"propertyTaxUsageNonResidential"`
	SewerageTaxOwner             float64 `json:"sewerageTax"`
	WasteTaxOnePersonHousehold   float64 `json:"wasteTaxOnePersonHousehold"`
	WasteTaxMultiPersonHousehold float64 `json:"wasteTaxmultiPersonHousehold"`
}

var specsKeyRE = regexp.MustCompile(`^specs\[(.+?)\]\[(.+?)\]$`)

func main() {
	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"numberFormat": helpers.NumberFormat,
		"formatTime":   helpers.FormatTime,
		"formatDate":   helpers.FormatDate,
		"formatYear":   helpers.FormatYear,
		"formatString": helpers.FormatString,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",

		// Override default error handler
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			data := fiber.Map{
				"title":   fmt.Sprintf("Fout %d", code),
				"code":    code,
				"details": err.Error(),
			}

			// If requested via HTMX, only return the error partial
			if len(c.Request().Header.Peek("HX-Request")) != 0 {
				if err = c.Render("partials/error-content", data, ""); err != nil { // Note: do not set the status (i.e. always return a 200 response), so HTMX will show the response. IMPROVE: listen to htmx:responseError events instead
					// In case serving the error partial fails
					return c.Status(code).SendString(err.Error())
				}

				return nil
			}

			// Otherwise, return a full custom error page
			if err = c.Status(code).Render("error", data); err != nil {
				// In case serving the error page fails
				return c.Status(code).SendString(err.Error())
			}

			return nil
		},
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// Batch runs
	batchRuns := app.Group("/batch-runs")

	app.Get("/", func(c *fiber.Ctx) error {
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.Run]
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("backend request: %w", err)
		}

		return c.Render("index", fiber.Map{
			"title":          "Batch runs",
			"municipalities": config.Config.Municipalities,
			"runs":           response.Data,
			"pagination":     response.Pagination,
		})
	})

	batchRuns.Get("/new", func(c *fiber.Ctx) error {
		return c.Render("batch-run-new", fiber.Map{
			"title":          "Nieuwe batch run",
			"municipalities": config.Config.Municipalities,
		})
	})

	batchRuns.Post("/", func(c *fiber.Ctx) error {
		data := new(struct {
			Year           int
			Municipalities []string
			Tariffs        map[string]specs
		})
		if err := c.BodyParser(data); err != nil {
			return err
		}

		// Note: there seems to be no easy way to decode the arbitrary keys using Fiber's BodyParser, so we decode the form data using url.ParseQuery
		values, err := url.ParseQuery(string(c.Body()))
		if err != nil {
			return err
		}

		data.Tariffs = make(map[string]specs)

		for key, vals := range values {
			switch key {
			case "year":
				if len(vals) != 0 {
					data.Year, _ = strconv.Atoi(vals[0])
				}

			case "municipalities":
				data.Municipalities = vals

			default:
				matches := specsKeyRE.FindStringSubmatch(key)
				if len(matches) != 3 { // Should never be the case because of the defined regexp, but checked to be sure to avoid panics
					continue
				}

				// In case no value is set, ignore this key
				if len(vals) == 0 {
					continue
				}

				municipality := matches[1]
				val, _ := strconv.ParseFloat(vals[0], 64)

				// Set and update the specs
				specs := data.Tariffs[municipality]
				switch matches[2] {
				case "property-tax-ownership-home":
					specs.PropertyTaxOwnershipHome = val

				case "property-tax-ownership-non-home":
					specs.PropertyTaxOwnershipNonHome = val

				case "property-tax-usage-non-home":
					specs.PropertyTaxUsageNonHome = val

				case "sewerage-tax-owner":
					specs.SewerageTaxOwner = val

				case "waste-tax-one-person-household":
					specs.WasteTaxOnePersonHousehold = val

				case "waste-tax-multi-person-household":
					specs.WasteTaxMultiPersonHousehold = val
				}

				data.Tariffs[municipality] = specs
			}
		}

		// Filter municipality rates
		for k := range data.Tariffs {
			found := false
			for idx := range data.Municipalities {
				if strings.Compare(data.Municipalities[idx], k) == 0 {
					found = true
					break
				}
			}

			if !found {
				delete(data.Tariffs, k)
			}
		}

		// Start the batch run
		var ider struct {
			ID uuid.UUID `json:"id"`
		}
		if err := backend.Request(http.MethodPost, config.Config.Backend.Domain, "/runs", data, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &ider); err != nil {
			return err
		}

		if err := session.Store(c, "run-id", ider.ID.String()); err != nil {
			return err
		}

		// Return with information about the number and duration of the results in a success message
		return c.Render("partials/ozb-new-listen", nil, "")
	})

	batchRuns.Get("/status", func(c *fiber.Ctx) error {
		c.Set("Content-Type", "text/event-stream")
		c.Set("Cache-Control", "no-cache")
		c.Set("Connection", "keep-alive")
		c.Set("Transfer-Encoding", "chunked")

		idStr, err := session.Peek(c, "run-id")
		if err != nil || idStr == nil {
			return fmt.Errorf("cannot find session")
		}

		id, err := uuid.Parse(idStr.(string))
		if err != nil {
			return fmt.Errorf("cannot parse uuid")
		}

		c.Context().SetBodyStreamWriter(fasthttp.StreamWriter(func(w *bufio.Writer) {
			n := time.NewTimer(1 * time.Millisecond)
			for {
				select {
				case <-n.C:
					var work model.Work
					if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs/%s/status", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &work); err != nil {
						fmt.Printf("backend request failed: %s", err)
						return
					}

					temp, err := template.New("ozb-new-listen-status.html").ParseFiles("views/partials/ozb-new-listen-status.html")
					if err != nil {
						fmt.Printf("template parse: %s", err)
						return
					}

					fmt.Fprint(w, "data:")

					if err := temp.Execute(w, map[string]any{"Done": work.Done, "Amount": work.Amount, "Error": work.Error, "Finished": work.Finished, "FinishedIn": work.FinishedIn.String()}); err != nil {
						if strings.Contains(err.Error(), "connection closed") {
							fmt.Printf("connection closed")
							return
						}

						fmt.Printf("template execute: %v", err)
						return
					}
					fmt.Fprint(w, "\n\n")

					w.Flush()

					if work.Finished {
						return
					}

					n = time.NewTimer(1 * time.Second)
				}
			}
		}))

		return nil
	})

	batchRuns.Get("/:id", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		// Get the run itself
		run := new(model.Run)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs/%s", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, run); err != nil {
			return fmt.Errorf("error fetching batch run: %w", err)
		}

		// Get the run status
		var work *model.Work
		if run.Status == model.RunStatusBatchRunRunning {
			if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs/%s/status", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &work); err != nil {
				fmt.Println(fmt.Errorf("error fetching batch run status: %w", err))
			}
		}

		// Get the run's tariffs
		tariffs := new([]model.Tariff)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs/%s/tariffs", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, tariffs); err != nil {
			return fmt.Errorf("error fetching batch run tariffs: %w", err)
		}

		return c.Render("batch-run-details", fiber.Map{
			"title":   fmt.Sprintf("Batch run %s", id.String()),
			"run":     run,
			"work":    work,
			"tariffs": tariffs,
		})
	})

	batchRuns.Post("/:id/finalize", func(c *fiber.Ctx) error {
		id, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return err
		}

		if err := session.Store(c, "finalize-run-id", id.String()); err != nil {
			return err
		}

		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/runs/%s/finalize", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, nil); err != nil {
			return err
		}

		return c.Render("partials/finalize-new-listen", nil, "")
	})

	app.Get("/finalize/status", func(c *fiber.Ctx) error {
		c.Set("Content-Type", "text/event-stream")
		c.Set("Cache-Control", "no-cache")
		c.Set("Connection", "keep-alive")
		c.Set("Transfer-Encoding", "chunked")

		idStr, err := session.Peek(c, "finalize-run-id")
		if err != nil || idStr == nil {
			return fmt.Errorf("cannot find session")
		}

		id, err := uuid.Parse(idStr.(string))
		if err != nil {
			return fmt.Errorf("cannot parse uuid")
		}

		c.Context().SetBodyStreamWriter(fasthttp.StreamWriter(func(w *bufio.Writer) {
			n := time.NewTimer(1 * time.Millisecond)
			for {
				select {
				case <-n.C:
					work := model.Work{}
					if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/tax-bills/%s/status", id), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &work); err != nil {
						fmt.Printf("backend request failed: %s", err)
						return
					}

					temp, err := template.New("ozb-new-listen-status.html").ParseFiles("views/partials/ozb-new-listen-status.html")
					if err != nil {
						fmt.Printf("template parse: %s", err)
						return
					}

					fmt.Fprint(w, "data:")
					if err := temp.Execute(w, map[string]any{"Done": work.Done, "Amount": work.Amount, "Error": work.Error, "Finished": work.Finished, "FinishedIn": work.FinishedIn.String()}); err != nil {
						if strings.Contains(err.Error(), "connection closed") {
							fmt.Printf("connection closed")
							return
						}
						fmt.Printf("template execute: %v", err)
					}
					fmt.Fprint(w, "\n\n")

					w.Flush()

					if work.Finished {
						return
					}

					n = time.NewTimer(1 * time.Second)
				}
			}
		}))

		return nil
	})
	// WOZ objects
	wozObjects := app.Group("/woz-objects")
	wozObjects.Get("/", func(c *fiber.Ctx) error {
		// Redirect to the first municipality in the config
		if len(config.Config.Municipalities) == 0 {
			return errors.New("no municipalities in config")
		}

		return c.Redirect(fmt.Sprintf("/woz-objects/%s", config.Config.Municipalities[0].ID))
	})

	wozObjects.Get("/:municipalityID", func(c *fiber.Ctx) error {
		// Get the selected municipality
		municipality, err := config.GetMunicipalityByID(c.Params("municipalityID"))
		if err != nil {
			return err
		}

		// Fetch the WOZ objects from the WOZ objects backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.WOZObject]
		if err := backend.Request(http.MethodGet, municipality.WOZ.Domain, fmt.Sprintf("/woz-objects?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": municipality.WOZ.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching WOZ objects: %v", err)
		}

		return c.Render("woz-object-index", fiber.Map{
			"title":          "WOZ-objecten",
			"municipality":   municipality,
			"municipalities": config.Config.Municipalities,
			"objects":        response.Data,
			"pagination":     response.Pagination,
		})
	})

	wozObjects.Get("/:municipalityID/:id", func(c *fiber.Ctx) error {
		// Fetch the WOZ object from the WOZ objects backend
		objectID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		// Get the selected municipality
		municipality, err := config.GetMunicipalityByID(c.Params("municipalityID"))
		if err != nil {
			return err
		}

		object := new(model.WOZObject)
		if err := backend.Request(http.MethodGet, municipality.WOZ.Domain, fmt.Sprintf("/woz-objects/%s", objectID), nil, map[string]string{"Fsc-Grant-Hash": municipality.WOZ.FSCGrantHash}, object); err != nil {
			return fmt.Errorf("WOZ object request failed: %w", err)
		}

		decisions := make([]model.Decision, 0)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/woz-objects/%s/decisions", objectID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &decisions); err != nil {
			return fmt.Errorf("woz decisions request failed: %w", err)
		}

		return c.Render("woz-object-details", fiber.Map{
			"title":        fmt.Sprintf("WOZ-object: %s", object.FormattedType()),
			"municipality": municipality,
			"object":       object,
			"decisions":    decisions,
		})
	})

	// Assessment lines
	app.Get("/lines", func(c *fiber.Ctx) error {
		// Fetch the tax assessments from the Tax assessments backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.TaxAssessment]
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/assessments?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching tax assessments: %v", err)
		}

		return c.Render("woz-line-index", fiber.Map{
			"title":       "Aanslagregels",
			"assessments": response.Data,
			"pagination":  response.Pagination,
		})
	})

	// Tax assessments
	assessments := app.Group("/assessments")
	assessments.Get("/", func(c *fiber.Ctx) error {
		// Fetch the tax assessments from the Tax assessments backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.TaxBill]
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/tax-bills?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching tax bills: %w", err)
		}

		return c.Render("woz-assessment-index", fiber.Map{
			"title":       "Aanslagbiljetten",
			"assessments": response.Data,
			"pagination":  response.Pagination,
		})
	})

	assessments.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the tax assessment from the Tax assessments backend
		assessmentID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		assessment := new(model.TaxAssessment)
		if err := backend.Request(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/assessments/%s", assessmentID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, assessment); err != nil {
			return fmt.Errorf("tax assessment request failed: %w", err)
		}

		return c.Render("woz-assessment-details", fiber.Map{
			"title":      "",
			"assessment": assessment,
		})
	})

	app.Get("/tax-bills/:id/download", func(c *fiber.Ctx) error {
		// Fetch the tax assessment from the Tax assessments backend
		taxBillID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		// Note: We cannot use a byte stream to download the PDF directly into the ResponseWriter
		// Because this is not support by fiber
		file, err := os.CreateTemp("/tmp/", "temp-*.pdf")
		if err != nil {
			return nil
		}

		defer os.Remove(file.Name())

		if err := backend.Download(http.MethodGet, config.Config.Backend.Domain, fmt.Sprintf("/tax-bills/%s/download", taxBillID), map[string]string{"Fsc-Grant-Hash": config.Config.Backend.FSCGrantHash}, file); err != nil {
			return fmt.Errorf("tax bill download request failed: %w", err)
		}

		return c.Download(file.Name(), fmt.Sprintf("aanslagbiljet-%s.pdf", time.Now().Format("2006-01-02T15:04:05")))
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
